// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:vsmapp/login.dart';

void main() {
  // var delegate = await LocalizationDelegate.create(
  //     fallbackLocale: 'en', supportedLocales: ['nl', 'en', 'ru']);
  Widget createWidgetForTesting(LocalizationDelegate delegate,
      {required Widget child}) {
    return LocalizedApp(
      delegate,
      MaterialApp(
        home: child,
      ),
    );
  }

  testWidgets('login ...', (WidgetTester tester) async {
    await tester.runAsync(() async {
      // ASSEMBLE

      var delegate = await LocalizationDelegate.create(
          fallbackLocale: 'en', supportedLocales: ['nl', 'en', 'ru']);
      await tester
          .pumpWidget(createWidgetForTesting(delegate, child: LoginScreen()));
      expect(find.text('Login'), findsOneWidget);
      expect(find.text('Fingerprint'), findsOneWidget);
      expect(find.text('Username'), findsOneWidget);
      expect(find.text('Password'), findsOneWidget);

      await tester.tap(find.text('Login'));
      await tester.pump();

      // expect(button, matcher)
      // expect(find, matcher)
    });
  });
}
