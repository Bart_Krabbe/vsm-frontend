// ignore_for_file: must_be_immutable, use_key_in_widget_constructors, avoid_print, prefer_const_constructors
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:vsmapp/custom_widgets/linecard.dart';
import 'package:vsmapp/providers/language.dart';
import 'custom_widgets/custombottembar.dart';
import 'objects/line.dart';
import 'globals.dart' as globals;

class LinesOverviewScreen extends StatefulWidget {
  @override
  State<LinesOverviewScreen> createState() => _LinesOverViewScreen();
}

class _LinesOverViewScreen extends State<LinesOverviewScreen> {
  List<Line> lines = [];
  LocalStorage storage = LocalStorage('vsm_app_storage');

  getLines() async {
    var token = await storage.getItem("TOKEN");
    if (token != null) {
      final response = await http.get(
        Uri.parse(globals.currentPlatform + '/line'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': token,
        },
      );

      if (response.statusCode == 200) {
        List<Line> tempLines = [];
        var r = jsonDecode(response.body);
        for (var i = 0; i < r.length; i++) {
          tempLines.add(Line.fromJson(r[i]));
        }
        setState(() {
          lines = tempLines;
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(response.statusCode.toString()),
        ));
      }
    } else {
      if (!Navigator.canPop(context)) {
        Navigator.popAndPushNamed(context, '/login');
      } else {
        Navigator.pop(context);
      }
    }
  }

  setLocalStorage(var lang) async {
    if (await storage.ready) {
      await storage.setItem('language', lang);
      setState(() {
        changeLocale(context, lang);
      });
    }
  }

  @override
  void initState() {
    getLines();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.landscape) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
          overlays: [SystemUiOverlay.bottom]);
    }
    return (Scaffold(
      appBar: AppBar(
        title: Text(translate('LinesPage.LinesOverview')),
        backgroundColor: Colors.red,
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: DropdownButton<Language>(
              underline: SizedBox(),
              icon: Icon(Icons.language, color: Colors.white),
              onChanged: (lang) {
                setLocalStorage(lang!.languageCode);
              },
              items: Language.languageList()
                  .map<DropdownMenuItem<Language>>(
                    (lang) => DropdownMenuItem<Language>(
                      value: lang,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            lang.flag,
                            style: TextStyle(fontSize: 30),
                          ),
                          Text(lang.name,
                              style: TextStyle(color: Colors.black)),
                        ],
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          MediaQuery.of(context).orientation == Orientation.portrait || kIsWeb
              ? GestureDetector(
                  onTap: () => Navigator.popAndPushNamed(context, '/login'),
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Icon(Icons.logout),
                  ),
                )
              : Text(""),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomLeft,
          colors: const [
            Colors.redAccent,
            Colors.white,
          ],
        )),
        child: Center(
          child: lines.isEmpty
              ? SizedBox(
                  width: 100,
                  height: 100,
                  child: const CircularProgressIndicator(
                    color: Colors.red,
                    backgroundColor: Colors.white,
                    strokeWidth: 7.0,
                  ),
                )
              : RefreshIndicator(
                  onRefresh: () => getLines(),
                  child: ListView(
                    children: lines
                        .map((e) => Container(
                              constraints: BoxConstraints(maxWidth: 50),
                              child: LineCard(
                                line: e,
                              ),
                            ))
                        .toList(),
                  ),
                ),
        ),
      ),
      bottomNavigationBar:
          MediaQuery.of(context).orientation == Orientation.portrait
              ? CustomBottemBar()
              : null,
    ));
  }
}
