// ignore_for_file: must_be_immutable, use_key_in_widget_constructors, avoid_print, avoid_web_libraries_in_flutter, prefer_const_constructors
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:vsmapp/objects/statuslistobject.dart';
import 'custom_widgets/custombottembar.dart';
import 'globals.dart' as globals;
import 'objects/machine.dart';
import 'package:pie_chart/pie_chart.dart';

import 'objects/statuslistobject.dart';

class MachineViewScreen extends StatefulWidget {
  @override
  State<MachineViewScreen> createState() => _MachineViewScreen();
}

class _MachineViewScreen extends State<MachineViewScreen> {
  Machine machine = Machine.newMachine();
  double rateone = 0;
  double ratetwo = 220;
  final LocalStorage storage = LocalStorage('vsm_app_storage');
  List<StatusListObject> listOfStatusses = [];
  // final channel = WebSocketChannel.connect(
  //   Uri.parse(''),
  // );

  getMachineById() async {
    var token = await storage.getItem('TOKEN');
    if (token == null) {
      if (!Navigator.canPop(context)) {
        Navigator.popAndPushNamed(context, '/login');
      } else {
        Navigator.pop(context);
      }
      return;
    }
    final response = await http.get(
      Uri.parse(
        globals.currentPlatform +
            '/line/machines/' +
            globals.currentLineId.toString() +
            '/' +
            globals.machineId.toString(),
      ),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );
    if (response.statusCode == 200) {
      var machine1 = Machine.fromJson(jsonDecode(response.body));
      setState(() {
        machine = machine1;
      });
      getStatussenInList();
    }
  }

  getStatussenInList() async {
    List<StatusListObject> listOfStatusses1 = [];
    var statussen = machine.machineStatusses;
    for (var i = 0; i < statussen.length; i++) {
      bool pass = true;
      for (StatusListObject statusO in listOfStatusses1) {
        // Check if list already has the object
        if (statusO.statusname == statussen[i].status) {
          statusO.amount += 1;
          pass = false;
          break;
        }
      }
      if (pass) {
        listOfStatusses1.add(
          StatusListObject(statussen[i].status, 1),
        );
      }
    }
    setState(() {
      listOfStatusses = listOfStatusses1;
    });
    // Might need next element in the list to calculate amount of hours for each status so dont use a for each loop.
    // for (var status in machine.machineStatusses) {
    //   if (listOfStatusses.isEmpty) {
    //     listOfStatusses
    //         .add(StatusListObject(statusname: status.status, amount: 1));
    //   }
    // }
    return 1;
  }

  Map<String, double> getDataMap() {
    Map<String, double> dataMap = {};
    for (var item in listOfStatusses) {
      dataMap.addAll({"" + item.statusname + "": item.amount.toDouble()});
    }
    return dataMap;
  }

  registerListener() {}

  @override
  void initState() {
    getMachineById();
    registerListener();
    super.initState();
  }

  @override
  void dispose() {
    // channel.sink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      appBar: AppBar(
        title: Text(machine.machineName),
        backgroundColor: Colors.red,
      ),
      body: NotificationListener(
        onNotification: (v) {
          try {
            if (v is ScrollUpdateNotification) {
              //only if scroll update notification is triggered
              if (v.scrollDelta != null) {
                setState(() {
                  rateone -= v.scrollDelta! / 1.5;
                  if (v.scrollDelta! < 50) {
                    ratetwo -= v.scrollDelta! / 1.5;
                  } else {
                    ratetwo == -20;
                  }
                });
              }
            }
          } catch (e) {
            throw NullThrownError();
          }
          return true;
        },
        child: machine.machineName == 'null'
            ? const CircularProgressIndicator(
                color: Colors.red,
                backgroundColor: Colors.white,
                strokeWidth: 7.0,
              )
            : Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    top: rateone - 40,
                    child: SizedBox(
                      height: 300,
                      child: Hero(
                        tag: machine.machineId.toString(),
                        child: Image(
                            image: AssetImage('assets/images/Voortman-' +
                                machine.machineName +
                                '.jpg'),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  ListView(
                    children: [
                      Container(
                        height: 220,
                        color: Colors.transparent,
                      ),
                      Text(machine.machineStatus),
                      listOfStatusses.isNotEmpty
                          ? Container(
                              decoration: BoxDecoration(
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.black,
                                    blurRadius: 10.0,
                                    spreadRadius: 0.1,
                                  ),
                                ],
                              ),
                              child: Card(
                                color: Colors.red[100],
                                child: Row(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.all(4.0),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(8.0),
                                        ),
                                        border: Border.all(
                                          width: 3,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            child: Text(
                                              "List of recent statusses",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            decoration: BoxDecoration(
                                              border: Border(
                                                bottom: BorderSide(
                                                  color: Colors.grey,
                                                  width: 2,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: listOfStatusses
                                                  .map(
                                                    (e) => Text(
                                                      e.statusname +
                                                          "\n" +
                                                          e.amount.toString() +
                                                          " times",
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                  )
                                                  .toList(),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 150,
                                      height: 150,
                                      child: PieChart(
                                        dataMap: getDataMap(),
                                        animationDuration:
                                            Duration(milliseconds: 800),
                                        // colorList: colorList,
                                        initialAngleInDegree: 0,
                                        chartType: ChartType.disc,
                                        chartRadius: 150,
                                        legendOptions: LegendOptions(
                                          showLegendsInRow: false,
                                          legendPosition: LegendPosition.top,
                                          showLegends: false,
                                          legendShape: BoxShape.circle,
                                          legendTextStyle: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 10,
                                          ),
                                        ),
                                        chartValuesOptions: ChartValuesOptions(
                                          chartValueBackgroundColor:
                                              Colors.red[100],
                                          showChartValueBackground: true,
                                          showChartValues: true,
                                          showChartValuesInPercentage: false,
                                          showChartValuesOutside: true,
                                          decimalPlaces: 0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Text("no data found"),
                      Container(
                        padding: const EdgeInsets.all(8.0),
                        margin: const EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                          border: Border.all(
                            color: Colors.grey,
                          ),
                        ),
                        child: const Text(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
                      ),
                    ],
                  ),
                ],
              ),
      ),
      bottomNavigationBar: CustomBottemBar(),
    ));
  }

  getHexCode(String statusname) {
    switch (statusname) {
      case 'RUNNING':
        return '#52BB1C';
      case 'ERROR':
        return '#FF0000';
      case 'WAITING FOR OPERATOR':
        return '#0000FF';
      default:
        return '#ffffff';
    }
  }
}

class PieData {
  String activity;
  double time;
  PieData(this.activity, this.time);
}

class ParallaxWidget extends StatelessWidget {
  const ParallaxWidget({
    required Key key,
    required this.top,
    required this.asset,
  }) : super(key: key);

  final double top;
  final String asset;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: -45,
      top: top,
      child: SizedBox(
        height: 550,
        width: 500,
        child: Image.asset("assets/$asset", fit: BoxFit.cover),
      ),
    );
  }
}
