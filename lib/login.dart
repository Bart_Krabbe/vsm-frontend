import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:localstorage/localstorage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'UI/input_field.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:flutter_translate/flutter_translate.dart';

import 'objects/token.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String username = 'bart';
  String password = 'bier';
  String localStorageToken = '';
  final LocalStorage storage = LocalStorage('vsm_app_storage');

  // checks the localstorage if there is a token available
  checkLocalStorage() async {
    if (await storage.ready) {
      var data = await storage.getItem('TOKEN');
      if (data != null) {
        setState(() {
          localStorageToken = data;
        });
      } else {
        setState(() {
          localStorageToken = 'not_found';
        });
      }
    }
  }

  // Sets the gotten token to the local storage
  setLocalStorage(String val) async {
    if (await storage.ready) {
      await storage.setItem('TOKEN', val);
      var t = await storage.getItem('TOKEN');
      setState(() {
        localStorageToken = t;
      });
    }
  }

  // Have to add logic when succesful fingerprint get local data, now it just sends you to the next page without checking
  loginWithFingerPrint() async {
    // Check if the app is running on the web
    if (!kIsWeb) {
      // Check if the app is running on android or ios
      if (Platform.isIOS || Platform.isAndroid) {
        LocalAuthentication localAuthentication = LocalAuthentication();
        // Check if we have acces to biometrics
        if (await localAuthentication.canCheckBiometrics &&
            await localAuthentication.isDeviceSupported()) {
          List<BiometricType> availableBiometrics =
              await localAuthentication.getAvailableBiometrics();
          // Checks if the app gives acces to the fingerprintscanner
          if (availableBiometrics.contains(BiometricType.fingerprint)) {
            await localAuthentication.stopAuthentication();
            // Login with biometrics, after succes returs true, else false
            bool didAuthenticate = await localAuthentication.authenticate(
                localizedReason: 'Please authenticate to log into the app!');
            if (didAuthenticate) {
              setState(() {
                globals.isLoggedIn = true;
              });
              Navigator.popAndPushNamed(context, '/lines');
            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Login using biometrics failed!'),
                ),
              );
            }
          }
        }
      }
    }
    return false;
  }

  // Login logic
  login() async {
    // Username and password are for now just openly available, because this is not a real implementation
    final response = await http.post(
      Uri.parse(globals.currentPlatform + '/account/login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'username': username,
        'password': password,
      }),
    );

    // Checks the statuscode to handle response
    if (response.statusCode == 200) {
      var token = Token.fromJson(jsonDecode(response.body));
      await setLocalStorage(token.token);
      Navigator.popAndPushNamed(context, '/lines');
      // Navigator.pushNamed(context, '/lines');
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(translate('LoginPage.loginFailed')),
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    checkLocalStorage();
    if (!kIsWeb) {
      if (Platform.isAndroid || Platform.isIOS) {
        setState(() {
          globals.currentPlatform = globals.localPhone;
        });
      }
    }
  }

  void _launchURL() async {
    try {
      await launch("https://www.voortmansteelgroup.com/");
    } catch (e) {
      throw 'Could not launch https://www.voortmansteelgroup.com/';
    }
  }

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      // Every screen needs a scaffold that has a body tag
      body: Container(
        // First child in this case is a container that makes up the background of the loginpage
        decoration: const BoxDecoration(
          // The boxdecoration creates the gradient background
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromRGBO(255, 255, 255, 0.4),
              Colors.orange,
              Colors.orange,
              Colors.red,
            ],
          ),
        ),
        // ordenary Width and Height
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        // Child variable is the main element that makes flutter awesome. Child says that the content after child: just takes place inside the parent. If you want content
        // to stack underneath use a column, list, stack instead
        child: Stack(
          // child of the stack is a list of widgets that makes up the login page content
          children: <Widget>[
            // Center is used to center horizontaly and verticaly
            Center(
              // Sized box is a sized box
              child: SizedBox(
                width: 400,
                height: 400,
                // Column stacks children underneath each other with the same logic as a flexbox
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    // Voortman Logo
                    Material(
                      elevation: 10.0,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(50.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onLongPress: _launchURL,
                          child: Image.asset(
                            "assets/images/voortman-logo-circle.png",
                            width: 80,
                            height: 80,
                          ),
                        ),
                      ),
                    ),
                    // Username field
                    Form(
                      child: InputField(
                        //Calling inputField  class
                        const Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                        translate('LoginPage.username'),
                        false,
                        callback: (String val) => setState(() {
                          username = val;
                        }),
                      ),
                    ),
                    // Password field
                    Form(
                      child: InputField(
                          const Icon(
                            Icons.lock,
                            color: Colors.white,
                          ),
                          translate('LoginPage.password'),
                          true,
                          callback: (String val) => setState(() {
                                password = val;
                              })),
                    ),
                    // Button container
                    SizedBox(
                      width: 200,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                              minimumSize: MaterialStateProperty.all<Size>(
                                  const Size(double.infinity, 40)),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10.0),
                                  ),
                                ),
                              ),
                              backgroundColor:
                                  MaterialStateProperty.all<Color>(Colors.red),
                            ),
                            onPressed: () {
                              login();
                            },
                            child: Text(
                              translate("LoginPage.login"),
                              style: const TextStyle(
                                fontSize: 20.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          !kIsWeb
                              ? ElevatedButton(
                                  style: ButtonStyle(
                                    minimumSize:
                                        MaterialStateProperty.all<Size>(
                                            const Size(double.infinity, 40)),
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10.0),
                                        ),
                                      ),
                                    ),
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.red),
                                  ),
                                  onPressed: localStorageToken == 'not_found'
                                      ? () {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(
                                              content: Text(
                                                translate(
                                                    'LoginPage.noTokenSetError'),
                                              ),
                                            ),
                                          );
                                        }
                                      : () {
                                          loginWithFingerPrint();
                                        },
                                  child: Text(
                                    translate("LoginPage.fingerprint"),
                                    style: const TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              : const Text(""),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
