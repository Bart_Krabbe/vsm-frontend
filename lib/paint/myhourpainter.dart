// ignore_for_file: avoid_print
import 'package:flutter/material.dart';
import 'package:vsmapp/objects/list_per_hour.dart';

class MyHourPainter extends CustomPainter {
  ListPerHour statusses;
  String previousStatus;
  MyHourPainter({required this.statusses, required this.previousStatus});

  @override
  void paint(Canvas canvas, Size size) {
    paintList(canvas, size, statusses, 0, previousStatus);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

getPaint(String status) {
  switch (status) {
    case 'RUNNING':
      return Paint()
        ..color = const Color(0xff52BB1C)
        ..style = PaintingStyle.fill;
    case 'ERROR':
      return Paint()
        ..color = const Color(0xffFF0000)
        ..style = PaintingStyle.fill;
    case 'WAITING FOR OPERATOR':
      return Paint()
        ..color = const Color(0xff0000FF)
        ..style = PaintingStyle.fill;
    default:
      return Paint()
        ..color = const Color(0xffffffff)
        ..style = PaintingStyle.fill;
  }
}

void paintList(Canvas canvas, Size size, ListPerHour listPerHour, double height,
    String previousStatus) {
  double h = 10;
  double previousX = 0.0;
  // Space per second
  double spacePerSecond = size.width / 60 / 60;
  var startOfDay = DateTime(2021, 10, 29, listPerHour.hour);
  // For each status in that hour
  for (var i = 0; i < listPerHour.list.length; i++) {
    var paint = getPaint(listPerHour.list[i].status);
    var duration = 0;
    // If this is the first loop create a new difference and timestamp
    if (i == 0) {
      double dura =
          listPerHour.list[i].timeStamp.difference(startOfDay).inSeconds *
              spacePerSecond;
      canvas.drawRect(
          Offset(previousX, 0) & Size(dura, h), getPaint(previousStatus));
      previousX += dura;
    }
    if (i == listPerHour.list.length - 1) {
      var endOfHour = DateTime(2021, 10, 29, listPerHour.hour + 1);
      duration =
          listPerHour.list[i].timeStamp.difference(endOfHour).inSeconds * -1;
      canvas.drawRect(
          Offset(previousX, 0) & Size(duration * spacePerSecond, h), paint);
    } else {
      duration = listPerHour.list[i].timeStamp
              .difference(listPerHour.list[i + 1].timeStamp)
              .inSeconds *
          -1;
      canvas.drawRect(
          Offset(previousX, 0) & Size(duration * spacePerSecond, h), paint);
    }
    previousX += ((duration * spacePerSecond));
  }

  int currentHour = listPerHour.hour;
  double spaceBetweenSeconds = size.width / 60 * 5;
  for (var i = 0; i <= 12; i++) {
    if (i % 3 == 0) {
      canvas.drawRect(
          Offset(i * spaceBetweenSeconds, height - 1) & Size(2, h + 2),
          Paint()
            ..color = const Color(0xff000000)
            ..style = PaintingStyle.fill);
      // Draw the timestamp:
      paintHoursText(currentHour, i, canvas, spaceBetweenSeconds, height);
    } else {
      if (i == 12) {
        paintHoursText(currentHour, i, canvas, spaceBetweenSeconds, height);
      } else if (i == 0) {
        paintHoursText(currentHour, i, canvas, spaceBetweenSeconds, height);
      }
      // canvas.drawRect(
      //     Offset(i * spaceBetweenSeconds, height) & Size(1, h),
      //     Paint()
      //       ..color = const Color(0xff000000)
      //       ..style = PaintingStyle.fill);
    }
  }
}

void paintHoursText(int currentHour, int i, Canvas canvas,
    double spaceBetweenSeconds, double height) {
  String time = "";
  if (i == 12) {
    time = (currentHour + 1).toString() + ":00";
  } else if (i == 0) {
    time = (currentHour).toString() + ":00";
  } else {
    time = currentHour.toString() + ":" + (i * 5).toString();
  }
  TextSpan span =
      TextSpan(text: time, style: const TextStyle(color: Colors.pink));
  TextPainter tp = TextPainter(
      text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr);
  tp.layout();
  tp.paint(canvas, Offset((i * spaceBetweenSeconds - 16), height + 10));
}
