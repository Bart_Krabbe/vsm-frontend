// ignore_for_file: avoid_print
import 'package:flutter/material.dart';
import 'package:vsmapp/objects/list_per_hour.dart';
import 'package:vsmapp/objects/machine_status.dart';

class MyDayPainter extends CustomPainter {
  List<MachineStatus> list;
  MyDayPainter(this.list);

  @override
  void paint(Canvas canvas, Size size) {
    paintList(canvas, size, list);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

getPaint(String status) {
  switch (status) {
    case 'RUNNING':
      return Paint()
        ..color = const Color(0xff52BB1C)
        ..style = PaintingStyle.fill;
    case 'ERROR':
      return Paint()
        ..color = const Color(0xffFF0000)
        ..style = PaintingStyle.fill;
    case 'WAITING FOR OPERATOR':
      return Paint()
        ..color = const Color(0xff0000FF)
        ..style = PaintingStyle.fill;
    default:
      return Paint()
        ..color = const Color(0xffffffff)
        ..style = PaintingStyle.fill;
  }
}

void paintList(
    Canvas canvas, Size size, List<MachineStatus> machineStatusList) {
  double previousX = 0.0;
  double h = 10.0;
  // Space per second
  double spacePerSecond = size.width / 24 / 60 / 60;
  var startOfDay = DateTime(2021, 10, 29, 0, 0, 0);
  for (var i = 0; i < machineStatusList.length; i++) {
    var paint = getPaint(machineStatusList[i].status);
    var duration = 0;
    /////////////////////
    if (i == 0) {
      double dura =
          machineStatusList[i].timeStamp.difference(startOfDay).inSeconds *
              spacePerSecond;
      canvas.drawRect(
          const Offset(0, 0) & Size(dura * spacePerSecond, h), getPaint(""));
      previousX = dura;
    }

    if (i == machineStatusList.length - 1) {
      var endOfDay = DateTime(2021, 10, 29, 23, 59, 59);
      duration =
          machineStatusList[i].timeStamp.difference(endOfDay).inSeconds * -1;
      canvas.drawRect(
          Offset(previousX, 0) & Size(duration * spacePerSecond, h), paint);
    } else {
      duration = machineStatusList[i]
              .timeStamp
              .difference(machineStatusList[i + 1].timeStamp)
              .inSeconds *
          -1;
      canvas.drawRect(
          Offset(previousX, 0) & Size(duration * spacePerSecond, h), paint);
    }
    previousX += ((duration * spacePerSecond));

    ///
  }
  double spaceBetweenHours = spacePerSecond * 60 * 60;
  for (var i = 0; i <= 24; i++) {
    paintHoursText(i, canvas, spaceBetweenHours, 10);
  }
}

void paintHoursText(
    int currentHour, Canvas canvas, double spaceBetweenHours, double height) {
  // Need to paint every hour of the day

  var time = (currentHour).toString() + ":00";
  TextSpan span = TextSpan(
      text: time, style: const TextStyle(color: Colors.pink, fontSize: 10));
  TextPainter tp = TextPainter(
      text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr);
  tp.layout();
  tp.paint(canvas, Offset((currentHour * spaceBetweenHours - 16), height + 10));
  canvas.drawRect(
      Offset(currentHour * spaceBetweenHours, 0) & const Size(2, 20),
      Paint()
        ..color = const Color(0xff000000)
        ..style = PaintingStyle.fill);
}

fillListOL(List<ListPerHour> listOL, List<MachineStatus> list) {
  for (var i = 0; i < list.length; i++) {
    listOL[list[i].timeStamp.hour].list.add(list[i]);
  }
  return listOL;
}
