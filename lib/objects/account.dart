class Account {
  final int accountId;

  final String accountName;

  final String password;

  final int companyId;

  Account(
      {required this.accountId,
      required this.accountName,
      required this.password,
      required this.companyId});

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
        accountId: json['accountId'],
        accountName: json['accountName'],
        password: json['password'],
        companyId: json['companyId']);
  }
}
