class StatusListObject {
  String statusname;
  int amount;
  // int hours;

  StatusListObject(this.statusname, this.amount);

  @override
  String toString() {
    return "Statusname: " + statusname + "\nAmount: " + amount.toString();
  }
}
