import 'package:vsmapp/objects/machine_status.dart';

class Machine {
  int machineId;

  String machineName;

  String machineDescription;

  String machineStatus;

  int lineId;

  int companyId;

  List<MachineStatus> machineStatusses;

  factory Machine.newMachine() {
    return Machine(
        machineId: 0,
        machineName: 'null',
        machineDescription: 'null',
        machineStatus: 'null',
        lineId: 0,
        companyId: 0,
        machineStatusses: []);
  }

  Machine(
      {required this.machineId,
      required this.machineName,
      required this.machineDescription,
      required this.machineStatus,
      required this.lineId,
      required this.companyId,
      required this.machineStatusses});

  factory Machine.fromJson(Map<String, dynamic> json) {
    List<MachineStatus> mach = [];
    for (var m in json['machineStatuses']) {
      mach.add(MachineStatus.fromJson(m));
    }
    return Machine(
        machineId: json['machineId'],
        machineName: json['machineName'],
        machineDescription: json['machineDescription'],
        machineStatus: json['machineStatus'],
        lineId: json['lineId'],
        companyId: json['companyId'],
        machineStatusses: mach);
  }
}
