class MachineStatus {
  final int statusId;

  final String status;

  final DateTime timeStamp;

  MachineStatus(
      {required this.statusId, required this.status, required this.timeStamp});

  factory MachineStatus.fromJson(Map<String, dynamic> json) {
    return MachineStatus(
        statusId: json['statusId'],
        status: json['status'],
        timeStamp: DateTime.parse(json['timeStamp']));
  }

  @override
  String toString() {
    return 'Status: ' + status + ' Time Stamp: ' + timeStamp.toString() + '\n';
  }
}
