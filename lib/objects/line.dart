import 'package:vsmapp/objects/machine.dart';

class Line {
  final int lineId;

  final String lineName;

  final String lineDescription;

  final List<Machine> machines;

  final int companyId;

  Line(
      {required this.lineId,
      required this.lineName,
      required this.lineDescription,
      required this.machines,
      required this.companyId});

  factory Line.fromJson(Map<String, dynamic> json) {
    List<Machine> mach = [];
    for (var m in json['machines']) {
      mach.add(Machine.fromJson(m));
    }
    return Line(
      lineId: json['lineId'],
      lineName: json['lineName'],
      lineDescription: json['lineDescription'],
      machines: mach,
      companyId: json['companyId'],
    );
  }
}
