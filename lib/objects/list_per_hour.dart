import 'machine_status.dart';

class ListPerHour {
  final int hour;

  final List<MachineStatus> list;

  ListPerHour({required this.hour, required this.list});

  @override
  String toString() {
    return 'Hour: ' + hour.toString() + ' List: ' + list.toString() + '\n';
  }
}
