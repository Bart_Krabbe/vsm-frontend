// ignore_for_file: use_key_in_widget_constructors, avoid_print, import_of_legacy_library_into_null_safe, prefer_const_constructors
import 'package:localstorage/localstorage.dart';
import 'package:vsmapp/lines.dart';
import 'dart:io';
import 'package:vsmapp/machineview.dart';
import 'package:vsmapp/services/PushNotificationService.dart';

import 'package:flutter/material.dart';
import 'lineoverview.dart';
import 'login.dart';

import 'package:flutter_translate/flutter_translate.dart';

LocalStorage storage = LocalStorage('vsm_app_storage');

// The main method is the entry point for the app
void main() async {
  // Need to say that we want to make calls to the internet
  HttpOverrides.global = MyHttpOverrides();
  // Give the default language settings
  var delegate = await LocalizationDelegate.create(
      fallbackLocale: 'en', supportedLocales: ['nl', 'en', 'ru']);
  // Run the app
  runApp(LocalizedApp(delegate, MyApp()));
  // Create the notification service
  await PushNotificationService().setupInteractedMessage();
}

// Creates the internet gateway to make calls to the backend
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  // Get the local language from the store, because we need to set it to correct the language
  getLocaleFromLocalStorage() async {
    // Check if the storage is ready to read/write
    if (await storage.ready) {
      // Get the language from the localstorage and check if its null else set the language
      var data = await storage.getItem('language');
      if (data != null) {
        print("Found locally: " + data);
        setState(() {
          changeLocale(context, data);
        });
      } else {
        print("Nothing found locally");
        setLocale();
      }
    }
  }

  // Set local language
  setLocale() async {
    if (await storage.ready) {
      await storage.setItem('language', getCurrentLocale()!.languageCode);
    }
  }

  // On first load of the page
  @override
  void initState() {
    getLocaleFromLocalStorage();
    super.initState();
  }

  // This widget is the root of your application.
  // The build method returns the webpage in components
  @override
  Widget build(BuildContext context) {
    // Need to wrap the app in a Localization Provider to work with different language standards
    return LocalizationProvider(
      state: LocalizationProvider.of(context).state,
      child: MaterialApp(
        // Type of app is Material so need to wrap the app aswell
        title: 'Voortman Steel Machinery',
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        // We want routing in the application, so we need to create this routing module
        initialRoute: '/login',
        routes: {
          // Login Screen
          '/login': (context) => LoginScreen(),
          // Lines
          '/lines': (context) => LinesOverviewScreen(),
          // Overview of a single Line
          '/line/overview': (context) => LineOverviewScreen(),
          // Overview of a single Machine
          '/machine/overview': (context) => MachineViewScreen()
        },
      ),
    );
  }
}
