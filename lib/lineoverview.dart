// ignore_for_file: must_be_immutable, use_key_in_widget_constructors, avoid_print, prefer_const_constructors
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:vsmapp/custom_widgets/infocard.dart';
import 'package:vsmapp/custom_widgets/linecard.dart';
import 'custom_widgets/custombottembar.dart';
import 'objects/machine.dart';
import 'globals.dart' as globals;

class LineOverviewScreen extends StatefulWidget {
  @override
  State<LineOverviewScreen> createState() => _LineOverViewScreen();
}

class _LineOverViewScreen extends State<LineOverviewScreen> {
  List<Machine> machines = [];
  final LocalStorage storage = LocalStorage('vsm_app_storage');

  getMachinesFromLine() async {
    List<Machine> machinesTemp = <Machine>[];
    var token = await storage.getItem('TOKEN');
    if (token == null) {
      if (!Navigator.canPop(context)) {
        Navigator.popAndPushNamed(context, '/login');
      } else {
        Navigator.pop(context);
      }
      return;
    }
    final response = await http.get(
      Uri.parse(globals.currentPlatform +
          '/line/machines/' +
          globals.currentLineId.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );
    if (response.statusCode == 200) {
      var r = jsonDecode(response.body);
      for (var i = 0; i < r.length; i++) {
        machinesTemp.add(Machine.fromJson(r[i]));
      }
      setState(() {
        machines = machinesTemp;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(response.statusCode.toString()),
      ));
    }
  }

  @override
  void initState() {
    getMachinesFromLine();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      appBar: AppBar(
        title: const Text('Line Overview'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: Column(
          children: [
            machines.isEmpty
                ? const CircularProgressIndicator(
                    color: Colors.red,
                    backgroundColor: Colors.white,
                    strokeWidth: 7.0,
                  )
                : ScrollConfiguration(
                    behavior: MyBehavior(),
                    child: Expanded(
                        child: RefreshIndicator(
                      onRefresh: () => getMachinesFromLine(),
                      child: ListView(
                        children: machines
                            .map(
                              (e) => InfoCard(machine: e),
                            )
                            .toList(),
                      ),
                    )),
                  ),
          ],
        ),
      ),
      bottomNavigationBar:
          MediaQuery.of(context).orientation == Orientation.portrait
              ? CustomBottemBar()
              : null,
    ));
  }
}
