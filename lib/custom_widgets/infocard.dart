// ignore_for_file: use_key_in_widget_constructors, must_be_immutable, avoid_print, prefer_const_constructors, prefer_const_literals_to_create_immutables
import 'package:flutter/material.dart';
import 'package:vsmapp/objects/list_per_hour.dart';
import 'package:vsmapp/objects/machine.dart';
import 'package:vsmapp/objects/machine_status.dart';
import 'package:vsmapp/paint/mydaypainter.dart';
import 'package:vsmapp/paint/myhourpainter.dart';
import '../globals.dart' as globals;

class InfoCard extends StatefulWidget {
  final Machine machine;
  const InfoCard({required this.machine});
  @override
  State<InfoCard> createState() => _InfoCard();
}

class _InfoCard extends State<InfoCard> {
  late int one = 0;
  goToMachineView(BuildContext context) {
    setState(() {
      globals.machineId = widget.machine.machineId;
    });
    Navigator.pushNamed(context, '/machine/overview');
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          splashColor: Colors.red,
          onTap: () => goToMachineView(context),
          child: Container(
            height: 100,
            margin: const EdgeInsets.all(8),
            child: Card(
              child: Row(
                children: [
                  Hero(
                    tag: widget.machine.machineId.toString(),
                    child: Image(
                        image: AssetImage('assets/images/Voortman-' +
                            widget.machine.machineName +
                            '.jpg')),
                  ),
                  Text(
                    widget.machine.machineName,
                    style: const TextStyle(fontSize: 28),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
            ),
          ),
        ),
        Center(
          child: Container(
            margin: EdgeInsets.only(left: 8.0, right: 8.0),
            width: double.infinity,
            height: 100,
            child: MediaQuery.of(context).orientation == Orientation.portrait
                ? ListView(
                    children: getListOfPainted(widget.machine.machineStatusses),
                  )
                : Container(
                    width: 400,
                    height: 50,
                    margin: EdgeInsets.only(left: 8.0, right: 8.0),
                    child: PaintedByDay(widget.machine.machineStatusses),
                  ),
          ),
        ),
      ],
    );
  }
}

List<PaintedByHour> getListOfPainted(List<MachineStatus> listOfStatussen) {
  List<PaintedByHour> pB = [];
  listOfStatussen = sortList(listOfStatussen);
  List<ListPerHour> listOfLists = makeListOfListsPerHour(listOfStatussen);
  String previousStatus = '';
  for (var i = 0; i < listOfLists.length; i++) {
    if (i != 0 || i == listOfLists.length - 1) {
      if (listOfLists[i - 1].list.isNotEmpty) {
        previousStatus =
            listOfLists[i - 1].list[listOfLists[i - 1].list.length - 1].status;
      }
    }
    if (listOfLists[i].list.isNotEmpty) {
      pB.add(PaintedByHour(listOfLists[i], previousStatus));
    }
  }
  return pB;
}

fillListOL(List<ListPerHour> listOL, List<MachineStatus> list) {
  for (var i = 0; i < list.length; i++) {
    listOL[list[i].timeStamp.hour].list.add(list[i]);
  }
  return listOL;
}

sortList(List<MachineStatus> listOfStatusses) {
  listOfStatusses.sort((a, b) => a.timeStamp.compareTo(b.timeStamp));
  return listOfStatusses;
}

makeListOfListsPerHour(List<MachineStatus> list) {
  // Create list with list each hour
  List<ListPerHour> listOL = [];
  for (var i = 0; i < 24; i++) {
    listOL.add(ListPerHour(hour: i, list: []));
  }
  // Fill listOfLists
  listOL = fillListOL(listOL, list);
  return listOL;
}

class PaintedByHour extends StatelessWidget {
  ListPerHour list;
  String previousStatus;
  PaintedByHour(this.list, this.previousStatus);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 400,
      height: 50,
      margin: EdgeInsets.only(left: 8.0, right: 8.0),
      child: CustomPaint(
        size: Size(360, 50),
        painter: MyHourPainter(statusses: list, previousStatus: previousStatus),
      ),
    );
  }
}

class PaintedByDay extends StatelessWidget {
  List<MachineStatus> list;
  PaintedByDay(this.list);

  @override
  Widget build(BuildContext context) {
    list = sortList(list);
    return Container(
      width: 1200,
      height: 50,
      margin: EdgeInsets.only(left: 8.0, right: 8.0),
      child: CustomPaint(
        size: Size(1200, 50),
        painter: MyDayPainter(list),
      ),
    );
  }
}
