// ignore_for_file: must_be_immutable, use_key_in_widget_constructors, avoid_print, prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../globals.dart' as globals;
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:vsmapp/services/PushNotificationService.dart';

class CustomBottemBar extends StatefulWidget {
  const CustomBottemBar({
    Key? key,
  }) : super(key: key);

  @override
  State<CustomBottemBar> createState() => _CustomBottemBarState();
}

class _CustomBottemBarState extends State<CustomBottemBar> {
  goToHomePage() {
    var route = ModalRoute.of(context);
    if (route != null) {
      if (route.settings.name != '/lines') {
        while (true) {
          if (!Navigator.canPop(context)) {
            break;
          } else {
            Navigator.pop(context);
          }
        }
      }
    }
  }

  popNavigation() {
    while (true) {
      if (!Navigator.canPop(context)) {
        Navigator.popAndPushNamed(context, '/login');
        break;
      } else {
        Navigator.pop(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ConvexAppBar(
      style: TabStyle.fixed,
      backgroundColor: Colors.red,
      items: [
        TabItem(
            icon: Icons.notifications,
            title: translate('LinesPage.notifications')),
        TabItem<Widget>(
            icon: CircleAvatar(
              radius: 20,
              backgroundImage: AssetImage(
                'assets/images/voortman-logo.png',
              ),
            ),
            title: translate('LinesPage.home')),
        TabItem(icon: Icons.logout, title: translate('LinesPage.logout')),
      ],
      initialActiveIndex: globals.activeIndex,
      onTap: (int i) => {
        setState(() {
          globals.activeIndex = i;
        }),
        if (i == 0)
          {
            showDialog(
              context: context,
              builder: (_) => AlertDialog(
                title: Text('Notifications'),
                content: Text('Do you want notifications enabled?'),
                actions: [
                  IconButton(
                    //Accept Notifications
                    onPressed: () => {
                      PushNotificationService()
                          .subscribeForNotification('notificaties'),
                      Navigator.pop(context)
                    },
                    icon: Icon(
                      Icons.check,
                      color: Colors.green,
                    ),
                  ),
                  IconButton(
                    // Cancel Notifications
                    onPressed: () => {
                      PushNotificationService()
                          .unSubscribeFromNotification('notificaties'),
                      Navigator.pop(context)
                    },
                    icon: Icon(Icons.close, color: Colors.red),
                  )
                ],
                actionsAlignment: MainAxisAlignment.spaceEvenly,
              ),
            )
          }
        else if (i == 2)
          {popNavigation()}
        else
          {goToHomePage()}
      },
    );
  }
}
