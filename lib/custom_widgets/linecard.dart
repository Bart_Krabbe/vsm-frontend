// ignore_for_file: use_key_in_widget_constructors, must_be_immutable, avoid_print, prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:vsmapp/objects/line.dart';
import '../globals.dart' as globals;

class LineCard extends StatefulWidget {
  final Line line;
  const LineCard({Key? key, required this.line}) : super(key: key);

  @override
  _LineCardState createState() => _LineCardState();
}

class _LineCardState extends State<LineCard> {
  goToLineOverview() {
    setState(() {
      globals.currentLineId = widget.line.lineId;
    });
    Navigator.pushNamed(context, '/line/overview');
  }

  goToMachineView(int machineId, int lineID) {
    setState(() {
      globals.currentLineId = lineID;
      globals.machineId = machineId;
    });
    Navigator.pushNamed(context, '/machine/overview');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: const <BoxShadow>[
          BoxShadow(
            color: Colors.black,
            blurRadius: 10,
            spreadRadius: 0.1,
            offset: Offset(0, 5),
          ),
        ],
      ),
      height: 200,
      margin: const EdgeInsets.all(16.0),
      child: Center(
        child: Material(
          borderRadius: BorderRadius.circular(20.0),
          child: InkWell(
            borderRadius: BorderRadius.circular(20.0),
            onTap: () => goToLineOverview(),
            child: Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  // Name
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset(
                            "assets/images/voortman-logo-circle.png",
                            width: 40,
                            height: 40,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            widget.line.lineName,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 28),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Description
                  Text(
                    widget.line.lineDescription,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(4.0),
                      margin: const EdgeInsets.only(bottom: 2.0),
                      child: ScrollConfiguration(
                        behavior: MyBehavior(),
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: widget.line.machines
                              .map(
                                (e) => Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () =>
                                        goToMachineView(e.machineId, e.lineId),
                                    child: Hero(
                                      tag: e.machineId.toString(),
                                      child: Container(
                                        height: 80,
                                        width: 120,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(8.0),
                                          ),
                                          border: Border.all(
                                            color: getColor(e.machineStatus),
                                            width: 2,
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.black.withOpacity(0.3),
                                              blurRadius: 5.0,
                                              // spreadRadius: 3,
                                            ),
                                          ],
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: AssetImage(
                                              'assets/images/Voortman-' +
                                                  e.machineName +
                                                  '.jpg',
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

Color getColor(String machineStatus) {
  Color c = Colors.white;
  switch (machineStatus) {
    case 'RUNNING':
      c = Colors.green;
      break;
    case 'ERROR':
      c = Colors.red;
      break;
    case 'WAITING FOR OPERATOR':
      c = Colors.blue;
      break;
    default:
      c = Colors.white;
      break;
  }
  c = c.withOpacity(0.3);
  return c;
}
